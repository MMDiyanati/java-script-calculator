var num1 = "";
var num2;
var op;

function one() {
    var num = 1;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(1)
    } else {
        $('#display').text(value + num);
    }
}

function two() {
    var num = 2;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(2)
    } else {
        $('#display').text(value + num);
    }
}

function three() {
    var num = 3;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(3)
    } else {
        $('#display').text(value + num);
    }
}

function four() {
    var num = 4;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(4)
    } else {
        $('#display').text(value + num);
    }
}

function five() {
    var num = 5;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(5)
    } else {
        $('#display').text(value + num);
    }
}

function six() {
    var num = 6;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(6)
    } else {
        $('#display').text(value + num);
    }
}

function seven() {
    var num = 7;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(7)
    } else {
        $('#display').text(value + num);
    }
}

function eight() {
    var num = 8;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(8)
    } else {
        $('#display').text(value + num);
    }
}

function nine() {
    var num = 9;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(9)
    } else {
        $('#display').text(value + num);
    }
}

function zero() {
    var num = 0;
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text(0)
    } else {
        $('#display').text(value + num);
    }
}

function dot() {
    num = '.';
    var value = $('#display').text();
    if ($('#display').text() == '0.0') {
        $('#display').text('0.')
    } else {
        $('#display').text(value.substr(0, value.length) + num);

    }
}

function bk() {
    var value = $('#display').text();
    if ($('#display').text() != '0.0') {
        $('#display').text(value.substring(0, (value.length - 1)))
    }
}

function mc() {
    $('#display').text('0.0');
    $('#result').text('result')
    num1 = ""
}

function sum() {
    op = '+';
    if (num1 == "") {
        num1 = $('#display').text();
    } else {
        res()
    }

    if ($('#display').text() != '0.0') {
        $('#display').text('0.0')
    }


}

function mine() {
    op = '-';
    if (num1 == "") {
        num1 = $('#display').text();
    } else {
        res()
    }

    if ($('#display').text() != '0.0') {
        $('#display').text('0.0')
    }
}

function multiple() {
    op = '*';
    if (num1 == "") {
        num1 = $('#display').text();
    } else {
        res()
    }

    if ($('#display').text() != '0.0') {
        $('#display').text('0.0')
    }

}

function dive() {
    op = '/';

    if ($('#display').text() == '0.0') {
        alert('لطفا مقدار را وارد نمایید')
    } else if (num1 == "") {
        num1 = $('#display').text()
    } else {
        res()
    }

    if ($('#display').text() != '0.0') {
        $('#display').text('0.0')
    }

}

function res() {

    if (op == '+') {
        num2 = $('#display').text();
        $('#result').text((Number(num1) + Number(num2)))

    }
    if (op == '-') {
        num2 = $('#display').text();
        $('#result').text((Number(num1) - Number(num2)))

    }
    if (op == '*') {
        num2 = $('#display').text();
        $('#result').text((Number(num1) * Number(num2)))

    }
    if (op == '/') {
        num2 = $('#display').text();
        $('#result').text((Number(num1) / Number(num2)))

    }

    num1 = $('#result').text()
    $('#display').text('0.0')
}

$(() => {

    $('#res').click(e => {
        $('res').animate({ opacity: '1', fontSize: '45px' }, 1500)
    });
});